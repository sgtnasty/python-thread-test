#!/usr/bin/env python3

import threading
import time
import sys
import random
import logging
import datetime
import argparse


MAXPROC = 4
TIMERS = [0.8, 0.9, 1.0, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2]


class Agent(threading.Thread):

    def __init__(self):
        super().__init__()
        self.stop = False
        self.count = 0
        logging.info(self)

    def run(self):
        while not self.stop:
            self.count += 1
            logging.info("tick {}".format(self.count))
            time.sleep(random.choice(TIMERS))
        logging.info("stop")


class Master(object):

    def __init__(self, args):
        self.args = args
        logging.info(self)
        self.stop = False
        self.procs = []
        self.start_time = None
        self.end_time = None

    def new_proc(self):
        self.procs.append(Agent())

    def start_procs(self):
        for proc in self.procs:
            proc.start()

    def stop_procs(self):
        for proc in self.procs:
            proc.stop = True

    def join_procs(self):
        for proc in self.procs:
            proc.stop = True
            proc.join()
            logging.info("{}.count = {}".format(proc, proc.count))

    def run(self):
        t = self.args.threads
        if t < MAXPROC:
            t = MAXPROC
        self.start_time = datetime.datetime.now()
        logging.info("creating {} new sub-processes...".format(t))
        for i in range(t):
            self.new_proc()
        logging.info("starting sub-processes...")
        self.start_procs()
        logging.info("main run loop")
        while not self.stop:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                logging.info("KeyboardInterrupt")
                self.stop = True
                self.stop_procs()
            except Exception as exp:
                logging.error(exp)
                sys.exit(1)
        self.join_procs()
        self.end_time = datetime.datetime.now()
        diff = self.end_time - self.start_time
        logging.info("runtime: {}".format(diff))


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.DEBUG,
        format='[%(levelname)s] (%(threadName)-10s) %(message)s')

    parser = argparse.ArgumentParser(description="Python thread test.")
    parser.add_argument("--threads", type=int, required=False, default=MAXPROC)
    args = parser.parse_args()

    if args.threads < MAXPROC:
        logging.error("Invalid number of threads: MIN=4")
        sys.exit(1)

    logging.info(parser.description)
    logging.info(repr(args))
    m = Master(args)
    m.run()
